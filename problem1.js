const fs = require("fs");
const path = require("path");
// Problem 1:
    
//     Using callbacks and the fs module's asynchronous functions, do the following:
//         1. Create a directory of random JSON files
//         2. Delete those files simultaneously


// 1. first create directory then in directory create random json files and then delete it sim
//ultaneously


function createDir(dir, cb){
    fs.mkdir(dir, err => {
        if(err){
            console.error(err);
        }
        else{
            console.log("Dir created");
            cb();
        }
    });
}



function writeFile(path, data, cb){
    fs.writeFile(path, data, err => {
        if(err){
            console.error(err);
        }
        else{
            console.log("file created");
            cb();
        }
    });
}



function deleteFile(dir){
    // read all files from directory it give array of all file names
    fs.readdir(dir, (err, files) => {
        if(err) throw err;
        // array of all files names
        // console.log(files);
       
        // iterate over the each file and send path to unlink for delete the file
        for(const file of files){
            fs.unlink(path.join(dir, file), err => {
                if(err) throw err;
                console.log("deleted");
            });
        }
    });
}

function func1(){
    const dir = path.join(__dirname,'random');
    createDir(dir, () => {
        writeFile((path.join(dir , 'file1.json')), 'abcabc', ()=>{
            writeFile((path.join(dir, 'file2.json')), 'abcabc', ()=> {
                writeFile((path.join(dir, 'file3.json')), 'abcabc',  ()=> {
                    deleteFile(dir);

                });
            });
        });
    });
}



// problem1();
module.exports = func1;

// remove files from the directory stackoverflow link
// https://stackoverflow.com/questions/27072866/how-to-remove-all-files-from-directory-without-removing-directory-in-node-js