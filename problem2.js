// Problem 2:

// Using callbacks and the fs module's asynchronous functions, do the following:
//     1. Read the given file lipsum.txt
//     2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//     3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//     4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//     5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
// */

// fs.readFile( filename, encoding, callback_function )

// Node.js program to demonstrate
// the fs.readFile() method
  
// 1. read file lipsum.txt  2. convert the content to uppercase and write to new file
// and save the name of the file also in fileName.txt
// read the new file and



const fs = require('fs');
  
// Use fs.readFile() method to read the file
// fs.readFile('lipsum.txt', 'utf8', function(err, data){
      
//     // Display the file content
//     console.log(data);
// });

const path = require("path");

function readFile(path, cb){
    fs.readFile(path, 'utf-8', (err, data) => {
        if(err){
            console.error(err);
        }
        else{
            cb(data);
        }
    });
}

function writeFile(path, data, cb){
    fs.writeFile(path, data, err => {
        if(err){
            console.error(err);
        }
        else{
            console.log("file created");
            cb();
        }
    });
}

function appendFile(path, data, cb){
fs.appendFile(path, data, err => {
    if(err)
    {
        console.log(err);
    }
    else
    {
        // console.log("file created and content added");
        cb();
    }
})

}




function deleteFiles(fileNamePath){
    readFile(fileNamePath, (data) => {
         let newdata = data.split(" ");
         for(let FileName of newdata){
            
                fs.unlink(path.join(__dirname, FileName), (err)=> {
                    if(err){
                        console.log(err);
                    }
                    console.log("file deleted success");
                })
            

            

            
         }
    })
}



function func2(){
    const lipsumFilePath = __dirname + '/lipsum.txt' ;
    const fileNamePath = __dirname + '/filenames.txt';
    // const fileNamePath = path.resolve(__dirname + 'filesnames.txt');
    readFile(lipsumFilePath, (data) => {
         const uppercaseFilePath = __dirname + '/uppercase.txt';
         writeFile(uppercaseFilePath, data.toUpperCase(), ()=> {
             appendFile(fileNamePath, "uppercase.txt", () => {
              readFile(uppercaseFilePath, (data) =>{
                  data = data.toLowerCase().replaceAll(". ","\n");
                  const lowercaseFilePath = __dirname + '/lowercase.txt';
                  writeFile(lowercaseFilePath, data, ()=>{
                    appendFile(fileNamePath, " lowercase.txt", ()=> {
                        readFile(lowercaseFilePath, (data) => {
                            data = data.split("\n").sort().join("\n");
                            const sortFilePath = __dirname + "/sortsentences.txt";
                            writeFile(sortFilePath, data, ()=>{
                                appendFile(fileNamePath, " sortsentences.txt", ()=> {
                                    deleteFiles(fileNamePath);
                                })
                            })
                            
                        })
                    })
                  })
                
              })

             })
             
             
         })


    })
}
  

module.exports = func2;